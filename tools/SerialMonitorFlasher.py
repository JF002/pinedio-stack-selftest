import serial
import os

serialPort = serial.Serial(port = "/dev/ttyUSB0", baudrate=2000000,bytesize=8, timeout=None, stopbits=serial.STOPBITS_ONE)
serialString = ""
resetCharCount = 0

while(True):
    serialString = serialPort.read()
    if serialString[0] == 255:
        resetCharCount = resetCharCount+1
        if resetCharCount == 2:
            serialPort.close()
            # FLASH
            os.system("/home/jf/bl602/flasher/bl602-flasher.py /dev/ttyUSB0 /home/jf/git/pinedio-stack-selftest/cmake-build-debug/pinedio-stack-selftest.bin")
            serialPort.open()
            resetCharCount = 0
    else:
        resetCharCount = 0
        # Print the contents of the serial data
        s = serialString.decode('ascii', errors='replace')
        print(s, end="")