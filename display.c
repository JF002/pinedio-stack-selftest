#include <bflb_platform.h>
#include "display.h"
#include "drivers/st7789.h"

Pinedio_ErrorCode_t display_color_result = PINEDIO_PENDING;

Pinedio_ErrorCode_t display_check() {
  printf("LCD\r\n");

  st7789_init();

  st7789_fill(0xFF00);
  bflb_platform_delay_ms(1000);

  st7789_fill(0x0FF0);
  bflb_platform_delay_ms(1000);

  st7789_fill(0x00FF);

  return display_color_result;
}

void display_set_color_ok() {
  display_color_result = PINEDIO_OK;
}

void display_set_color_error() {
  display_color_result = PINEDIO_ERROR;
}

Pinedio_ErrorCode_t display_get_test_result() {
  return display_color_result;
}