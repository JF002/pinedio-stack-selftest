#ifndef PINEDIO_STACK_SELFTEST_UI_H
#define PINEDIO_STACK_SELFTEST_UI_H

#include <stdint-gcc.h>
void ui_init();
void ui_on_touch_event(int16_t x, int16_t y);

#endif // PINEDIO_STACK_SELFTEST_UI_H
