#ifndef __PINEDIO_SYSTEMINFO__
#define __PINEDIO_SYSTEMINFO__

#include "pinedio.h"
Pinedio_ErrorCode_t systeminfo_get_system_info(void);

const char* systeminfo_get_info_ext();
const char* systeminfo_get_mcu_info();
const char* systeminfo_get_pin_info();
const char* systeminfo_get_memory_info();
Pinedio_ErrorCode_t systeminfo_get_result();
const char* systeminfo_get_wifi_mac();
#endif