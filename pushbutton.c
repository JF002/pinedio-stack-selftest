#include <bflb_platform.h>
#include "pushbutton.h"
#include "bl602_gpio.h"
#include "bl602_glb.h"
#include "hal_gpio.h"

Pinedio_ErrorCode_t pushbutton_result = PINEDIO_PENDING;
uint16_t pushbutton_count = 0;
uint32_t pushbutton_tick = 0;
uint32_t pushbutton_lastTick = 0;

static void pushbutton_on_interrupt() {
  if(pushbutton_tick - pushbutton_lastTick < 200) return; // Basic debounce

  pushbutton_count++;
  pushbutton_lastTick = pushbutton_tick;
  if(pushbutton_count < 3) {
    pushbutton_result = PINEDIO_PENDING;
  } else {
    pushbutton_result = PINEDIO_OK;
  }
}

void systick_isr() {
  // 1 tick = 1ms
  pushbutton_tick++;
}

void pushbutton_init() {
  printf("Push button\r\n");

  gpio_set_mode(PINEDIO_PIN_PUSHBUTTON, GPIO_SYNC_FALLING_TRIGER_INT_MODE);
  gpio_attach_irq(PINEDIO_PIN_PUSHBUTTON, pushbutton_on_interrupt);
  gpio_irq_enable(PINEDIO_PIN_PUSHBUTTON, ENABLE);

  pushbutton_result = PINEDIO_PENDING;

  bflb_platform_set_alarm_time(1000, systick_isr);
}

Pinedio_ErrorCode_t pushbutton_get_test_result() {
  return pushbutton_result;
}

void pushbutton_enable() {
  gpio_irq_enable(PINEDIO_PIN_PUSHBUTTON, ENABLE);
  pushbutton_count = 0;
}

void pushbutton_disable() {
  gpio_irq_enable(PINEDIO_PIN_PUSHBUTTON, DISABLE);
}

size_t pushbutton_get_nb_press() {
  return pushbutton_count;
}