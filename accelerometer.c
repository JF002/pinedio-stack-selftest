#include "accelerometer.h"
#include "drivers/i2c.h"
#include <bflb_platform.h>

#define PINEDIO_ACC_I2C_ADDRESS 0x4C

#define PINEDIO_ACC_REGISTER_STATUS 0x05
#define PINEDIO_ACC_REGISTER_MODE 0x07
#define PINEDIO_ACC_REGISTER_X_LSB 0x0D
#define PINEDIO_ACC_REGISTER_Y_LSB 0x0F
#define PINEDIO_ACC_REGISTER_Z_LSB 0x11

#define PINEDIO_ACC_CMD_STANDBYMODE 0x00
#define PINEDIO_ACC_CMD_WAKEMODE 0x01

Pinedio_ErrorCode_t accelerometer_test_result = PINEDIO_PENDING;

int16_t accelerometer_max_x = INT16_MIN;
int16_t accelerometer_max_y = INT16_MIN;
int16_t accelerometer_max_z = INT16_MIN;
int16_t accelerometer_min_x = INT16_MAX;
int16_t accelerometer_min_y = INT16_MAX;
int16_t accelerometer_min_z = INT16_MAX;
uint32_t accelerometer_read_count = 0;

void accelerometer_display_buffer(const char* name, const uint8_t data[], size_t size) {
  printf("%s : ", name);
  for(int i = 0; i < size; i++) {
    printf("0x%02x ", data[i]);
  }
  printf("\r\n");
}

Pinedio_ErrorCode_t accelerometer_check() {
  printf("Accelerometer\r\n");

  uint8_t status;

  uint8_t command = PINEDIO_ACC_CMD_STANDBYMODE;
  i2c_write(PINEDIO_ACC_I2C_ADDRESS, PINEDIO_ACC_REGISTER_MODE, &command, 1);
  bflb_platform_delay_ms(10);
  i2c_read(PINEDIO_ACC_I2C_ADDRESS, PINEDIO_ACC_REGISTER_STATUS, &status, 1);
  printf("\t Status register : %d [%s]\r\n", status, ((status==0)?"OK":"ERROR"));
  if(status != 0)
    return PINEDIO_ERROR;

  command = PINEDIO_ACC_CMD_WAKEMODE;
  i2c_write(PINEDIO_ACC_I2C_ADDRESS, PINEDIO_ACC_REGISTER_MODE, &command, 1);
  bflb_platform_delay_ms(10);
  i2c_read(PINEDIO_ACC_I2C_ADDRESS, PINEDIO_ACC_REGISTER_STATUS, &status, 1);
  printf("\t Status register : %d [%s]\r\n", status, ((status==0x01)?"OK":"ERROR"));
  if(status != 1)
    return PINEDIO_ERROR;

  return PINEDIO_OK;
}

void accelerometer_get_raw_values(struct accelerometer_raw_values_t* values) {
  i2c_read(PINEDIO_ACC_I2C_ADDRESS, PINEDIO_ACC_REGISTER_X_LSB, (uint8_t *)values, 6);

  if(accelerometer_read_count > 0) {
    if(values->x > accelerometer_max_x) accelerometer_max_x = values->x;
    if(values->y > accelerometer_max_y) accelerometer_max_y = values->y;
    if(values->z > accelerometer_max_z) accelerometer_max_z = values->z;

    if(values->x < accelerometer_min_x) accelerometer_min_x = values->x;
    if(values->y < accelerometer_min_y) accelerometer_min_y = values->y;
    if(values->z < accelerometer_min_z) accelerometer_min_z = values->z;

    if(accelerometer_max_x - accelerometer_min_x > 3000 &&
        accelerometer_max_y - accelerometer_min_y > 3000 &&
        accelerometer_max_z - accelerometer_min_z > 3000) {
      accelerometer_test_result = PINEDIO_OK;
    }
  }
  accelerometer_read_count++;
}

Pinedio_ErrorCode_t accelerometer_get_test_result() {
  return accelerometer_test_result;
}

void accelerometer_init() {
  accelerometer_test_result = PINEDIO_PENDING;
  accelerometer_read_count = 0;
}

