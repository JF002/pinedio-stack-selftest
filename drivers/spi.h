#ifndef PINEDIO_STACK_SELFTEST_SPI_H
#define PINEDIO_STACK_SELFTEST_SPI_H

#include "bl602_gpio.h"
#include "bl602_spi.h"
void spi_init(bool need_cs, bool need_swap, bool need_miso, SPI_CLK_POLARITY_Type clkPolarity);
void spi_transfer(GLB_GPIO_Type pinCS, uint8_t* cmd, size_t cmdSize, uint8_t* data, size_t dataSize);
void spi_write(GLB_GPIO_Type pinCS, uint8_t* data, size_t size);

#endif // PINEDIO_STACK_SELFTEST_SPI_H
