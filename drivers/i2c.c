#include <bflb_platform.h>
#include "i2c.h"
#include "bl602_i2c.h"
#include "bl602_glb.h"
#include "../pinedio.h"

void i2c_init() {
  uint8_t gpio_pins[] = {
      PINEDIO_PIN_I2C_SDA, PINEDIO_PIN_I2C_SCL
  };
  GLB_GPIO_Func_Init(GPIO_FUN_I2C, (GLB_GPIO_Type*)gpio_pins, 2);
}

void i2c_scan() {
  printf("I2C scan : \r\n");

  uint8_t a = 0;
  for(int i = 0; i < 127; i++) {
    I2C_Transfer_Cfg cfg;
    cfg.data = &a;
    cfg.dataSize = 1;
    cfg.slaveAddr = i;
    cfg.stopEveryByte = ENABLE;
    cfg.subAddr = 0;
    cfg.subAddrSize = 0;

    I2C_Reset(I2C0_ID);
    I2C_ClockSet(I2C0_ID, 800000);

    BL_Err_Type ret = I2C_MasterReceiveBlocking(I2C0_ID, &cfg);
    if(ret == SUCCESS) {
      char* dev;
      switch(i) {
      case 0x4c: dev = "MC3416 (G-Sensor)"; break;
      case 0x15: dev = "CST816S (Touch panel)"; break; // Won't be detected if it's in sleep mode. Touch the panel before running the test
      default: dev = "Unknown"; break;
      }

      printf("\t I2C device found at address 0x%02x : %s\r\n", i, dev);
    }
    bflb_platform_delay_ms(5);
  }
}

Pinedio_ErrorCode_t i2c_read(uint8_t deviceAddress, uint8_t registerAddress, uint8_t* data, uint16_t size) {
  I2C_Transfer_Cfg cfg;
  cfg.data = data;
  cfg.dataSize = size;
  cfg.slaveAddr = deviceAddress;
  cfg.stopEveryByte = ENABLE;
  cfg.subAddr = registerAddress;
  cfg.subAddrSize = 1;

  BL_Err_Type ret = I2C_MasterReceiveBlocking(I2C0_ID, &cfg);
  if(ret == SUCCESS)
    return PINEDIO_OK;
  return PINEDIO_ERROR;
}

Pinedio_ErrorCode_t i2c_write(uint8_t deviceAddress, uint8_t registerAddress, uint8_t* data, uint16_t size) {
  I2C_Transfer_Cfg cfg;
  cfg.data = data;
  cfg.dataSize = size;
  cfg.slaveAddr = deviceAddress;
  cfg.stopEveryByte = ENABLE;
  cfg.subAddr = registerAddress;
  cfg.subAddrSize = 1;

  BL_Err_Type ret = I2C_MasterSendBlocking(I2C0_ID, &cfg);
  if(ret == SUCCESS)
    return PINEDIO_OK;
  return PINEDIO_ERROR;
}