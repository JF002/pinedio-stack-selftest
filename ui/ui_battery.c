#include <lv_core/lv_obj.h>
#include <lv_core/lv_disp.h>
#include <lv_widgets/lv_label.h>
#include <lv_widgets/lv_btn.h>
#include "ui_battery.h"
#include "../battery.h"
#include "ui_main_menu.h"

lv_obj_t* ui_battery_label_v;
lv_obj_t* ui_battery_label_charge;
lv_obj_t*  ui_battery_btn;

static void ui_battery_on_event(lv_obj_t * obj, lv_event_t event) {
  if(obj == ui_battery_btn && event == LV_EVENT_PRESSED) {
    ui_main_menu_draw();
  }
}

void ui_battery_draw() {
  lv_obj_clean(lv_scr_act());

  lv_obj_t* label = lv_label_create(lv_scr_act(), NULL);
  lv_label_set_text(label, "Battery");
  lv_label_set_long_mode(label, LV_LABEL_LONG_BREAK);
  lv_label_set_align(label, LV_LABEL_ALIGN_CENTER);
  lv_obj_set_width(label, 240);
  lv_obj_align(label, NULL, LV_ALIGN_IN_TOP_MID, 0, 5);

  ui_battery_label_v = lv_label_create(lv_scr_act(), NULL);
  lv_label_set_text(ui_battery_label_v, "0 v");
  lv_label_set_long_mode(ui_battery_label_v, LV_LABEL_LONG_BREAK);
  lv_label_set_align(ui_battery_label_v, LV_LABEL_ALIGN_LEFT);
  lv_obj_align(ui_battery_label_v, label, LV_ALIGN_OUT_BOTTOM_LEFT, 0, 5);
  lv_obj_set_width(ui_battery_label_v, 240);


  ui_battery_label_charge = lv_label_create(lv_scr_act(), NULL);
  lv_label_set_text(ui_battery_label_charge, "Charging : ...");
  lv_label_set_long_mode(ui_battery_label_charge, LV_LABEL_LONG_BREAK);
  lv_label_set_align(ui_battery_label_charge, LV_LABEL_ALIGN_LEFT);
  lv_obj_align(ui_battery_label_charge, ui_battery_label_v, LV_ALIGN_OUT_BOTTOM_LEFT, 0, 5);
  lv_obj_set_width(ui_battery_label_charge, 240);

  ui_battery_btn = lv_btn_create(lv_scr_act(), NULL);
  lv_obj_set_style_local_value_str(ui_battery_btn, LV_BTN_PART_MAIN, LV_STATE_DEFAULT, "OK");
  lv_obj_set_event_cb(ui_battery_btn, ui_battery_on_event);
  lv_obj_align(ui_battery_btn, NULL, LV_ALIGN_IN_BOTTOM_MID, 0, -50);
  lv_obj_set_height(ui_battery_btn, 50);

  battery_init();
}

void ui_battery_refresh() {
  float b = battery_get_voltage();
  lv_label_set_text_fmt(ui_battery_label_v, "%0.2f v", b);

  if(battery_is_charging()) {
    lv_label_set_text(ui_battery_label_charge, "Charging : yes");
  } else {
    lv_label_set_text(ui_battery_label_charge, "Charging : no");
  }

}