#include <lv_core/lv_obj.h>
#include <lv_core/lv_disp.h>
#include <lv_widgets/lv_label.h>
#include <lv_widgets/lv_btn.h>
#include "ui_touchpanel.h"
#include "ui_main_menu.h"
#include "../touchpanel.h"

lv_obj_t* ui_pushbutton_btn_ok;
lv_obj_t* ui_pushbutton_btn1;
lv_obj_t* ui_pushbutton_btn2;
lv_obj_t* ui_pushbutton_btn3;

lv_obj_t* label_result;

static void ui_touchpanel__on_event(lv_obj_t * obj, lv_event_t event) {
  if(event != LV_EVENT_PRESSED) return;
  if(obj == ui_pushbutton_btn_ok) {
    ui_main_menu_draw();
  } else if(obj == ui_pushbutton_btn1) {
    touchpanel_on_top_left_tapped();
    lv_obj_set_style_local_value_color(obj, LV_BTN_PART_MAIN, LV_STATE_DEFAULT, LV_COLOR_LIME);
  } else if(obj == ui_pushbutton_btn2) {
    touchpanel_on_top_right_tapped();
    lv_obj_set_style_local_value_color(obj, LV_BTN_PART_MAIN, LV_STATE_DEFAULT, LV_COLOR_LIME);
  } else if(obj == ui_pushbutton_btn3) {
    touchpanel_on_bottom_left_tapped();
    lv_obj_set_style_local_value_color(obj, LV_BTN_PART_MAIN, LV_STATE_DEFAULT, LV_COLOR_LIME);
  }
}

void ui_touchpanel_draw() {
  lv_obj_clean(lv_scr_act());

  lv_obj_t* label = lv_label_create(lv_scr_act(), NULL);
  lv_label_set_text(label, "Please tap the 3 corners of the screen");
  lv_label_set_long_mode(label, LV_LABEL_LONG_BREAK);
  lv_label_set_align(label, LV_LABEL_ALIGN_CENTER);
  lv_obj_set_width(label, 240);
  lv_obj_align(label, NULL, LV_ALIGN_CENTER, 0, -30);

  label_result = lv_label_create(lv_scr_act(), NULL);
  lv_label_set_text(label_result, "");
  lv_obj_set_style_local_text_color(label_result, LV_BTN_PART_MAIN, LV_STATE_DEFAULT, LV_COLOR_GREEN);
  lv_label_set_align(label_result, LV_LABEL_ALIGN_CENTER);
  lv_obj_align(label_result, label, LV_ALIGN_OUT_BOTTOM_MID, 0, 5);

  ui_pushbutton_btn1 = lv_btn_create(lv_scr_act(), NULL);
  lv_obj_set_style_local_value_str(ui_pushbutton_btn1, LV_BTN_PART_MAIN, LV_STATE_DEFAULT, "TAP");
  lv_obj_set_event_cb(ui_pushbutton_btn1, ui_touchpanel__on_event);
  lv_obj_align(ui_pushbutton_btn1, NULL, LV_ALIGN_IN_TOP_LEFT, 0, 0);
  lv_obj_set_height(ui_pushbutton_btn1, 50);

  ui_pushbutton_btn2 = lv_btn_create(lv_scr_act(), NULL);
  lv_obj_set_style_local_value_str(ui_pushbutton_btn2, LV_BTN_PART_MAIN, LV_STATE_DEFAULT, "TAP");
  lv_obj_set_event_cb(ui_pushbutton_btn2, ui_touchpanel__on_event);
  lv_obj_align(ui_pushbutton_btn2, NULL, LV_ALIGN_IN_TOP_RIGHT, 0, 0);
  lv_obj_set_height(ui_pushbutton_btn2, 50);

  ui_pushbutton_btn3 = lv_btn_create(lv_scr_act(), NULL);
  lv_obj_set_style_local_value_str(ui_pushbutton_btn3, LV_BTN_PART_MAIN, LV_STATE_DEFAULT, "TAP");
  lv_obj_set_event_cb(ui_pushbutton_btn3, ui_touchpanel__on_event);
  lv_obj_align(ui_pushbutton_btn3, NULL, LV_ALIGN_IN_BOTTOM_LEFT, 0, -35);
  lv_obj_set_height(ui_pushbutton_btn3, 50);

  ui_pushbutton_btn_ok = lv_btn_create(lv_scr_act(), NULL);
  lv_obj_set_style_local_value_str(ui_pushbutton_btn_ok, LV_BTN_PART_MAIN, LV_STATE_DEFAULT, "OK");
  lv_obj_set_event_cb(ui_pushbutton_btn_ok, ui_touchpanel__on_event);
  lv_obj_align(ui_pushbutton_btn_ok, NULL, LV_ALIGN_IN_BOTTOM_MID, 0, -50);
  lv_obj_set_height(ui_pushbutton_btn_ok, 50);
}

void ui_touchpanel_refresh() {
  if(touchpanel_get_test_result() == PINEDIO_OK) {
    lv_label_set_text(label_result, LV_SYMBOL_OK);
  }
}