#include <lv_core/lv_obj.h>
#include <lv_widgets/lv_label.h>
#include <lv_core/lv_disp.h>
#include <lv_widgets/lv_led.h>
#include <lv_widgets/lv_btn.h>
#include "ui_pushbutton.h"
#include "../pushbutton.h"
#include "ui_main_menu.h"

lv_obj_t* ui_pushbutton_led;
lv_obj_t* ui_pushbutton_btn;
lv_obj_t* label_count;

static void ui_pushbutton_on_event(lv_obj_t * obj, lv_event_t event) {
  if(obj == ui_pushbutton_btn && event == LV_EVENT_PRESSED) {
    pushbutton_disable();
    ui_main_menu_draw();
  }
}

void ui_pushbutton_draw() {
  lv_obj_clean(lv_scr_act());

  lv_obj_t* label = lv_label_create(lv_scr_act(), NULL);
  lv_label_set_text(label, "Please press the button 3 times");
  lv_label_set_long_mode(label, LV_LABEL_LONG_BREAK);
  lv_label_set_align(label, LV_LABEL_ALIGN_CENTER);
  lv_obj_set_width(label, 240);
  lv_obj_align(label, NULL, LV_ALIGN_IN_TOP_MID, 0, 10);

  label_count = lv_label_create(lv_scr_act(), NULL);
  lv_label_set_text(label_count, "");
  lv_obj_align(label_count, label, LV_ALIGN_OUT_BOTTOM_MID, 0, 10);

  ui_pushbutton_led = lv_led_create(lv_scr_act(), NULL);
  lv_obj_align(ui_pushbutton_led, NULL, LV_ALIGN_CENTER, 0, 0);
  lv_led_off(ui_pushbutton_led);

  ui_pushbutton_btn = lv_btn_create(lv_scr_act(), NULL);
  lv_obj_set_style_local_value_str(ui_pushbutton_btn, LV_BTN_PART_MAIN, LV_STATE_DEFAULT, "OK");
  lv_obj_set_event_cb(ui_pushbutton_btn, ui_pushbutton_on_event);
  lv_obj_align(ui_pushbutton_btn, NULL, LV_ALIGN_IN_BOTTOM_MID, 0, -50);
  lv_obj_set_height(ui_pushbutton_btn, 50);

  pushbutton_init();
  pushbutton_enable();
}

void ui_pushbutton_refresh() {
  if(pushbutton_get_test_result() == PINEDIO_OK) {
    lv_led_on(ui_pushbutton_led);
  }
  else {
   lv_led_off(ui_pushbutton_led);
  }
  lv_label_set_text_fmt(label_count, "%d", pushbutton_get_nb_press());
}